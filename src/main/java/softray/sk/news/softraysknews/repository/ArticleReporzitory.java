package softray.sk.news.softraysknews.repository;

import softray.sk.news.softraysknews.models.Article;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ArticleReporzitory extends JpaRepository<Article,Long> {
    void deleteArticleById(Long id);

    Optional<Article> findArticleById(Long id);
}
