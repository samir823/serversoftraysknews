package softray.sk.news.softraysknews.security.services;


import softray.sk.news.softraysknews.exception.ArticleNotFoundException;
import softray.sk.news.softraysknews.models.Article;
import softray.sk.news.softraysknews.repository.ArticleReporzitory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Service
@Transactional
public class ArticleService {
    private final ArticleReporzitory articleRepo;

    @Autowired
    public ArticleService(ArticleReporzitory articleRepo) {
        this.articleRepo = articleRepo;
    }
    public Article addArticle(Article article){
        article.setCode(UUID.randomUUID().toString());
        return articleRepo.save(article);
    }
    public List<Article> findAllArticles() {
        return articleRepo.findAll();
    }
    public Article updateArticle(Article article){
        return articleRepo.save(article);
    }
    public void deleteArticle(Long id){
        articleRepo.deleteArticleById(id);
    }
    public Article findArticleById(Long id) {
        /*if we don't find Article*/
        return articleRepo.findArticleById(id).orElseThrow(()-> new ArticleNotFoundException("Article by id " + id+ "was not found"));
    }
}
