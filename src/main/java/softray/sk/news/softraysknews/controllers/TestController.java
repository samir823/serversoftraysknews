package softray.sk.news.softraysknews.controllers;

import softray.sk.news.softraysknews.security.services.ArticleService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//@CrossOrigin(origins = "*", maxAge = 3600)
@CrossOrigin(origins = "https://softray-cli.web.app", maxAge = 3600)
@RestController
@RequestMapping("/api/test")
public class TestController {
//	private final ArticleService articleService;

//	ArticleController articleController;

	public TestController(ArticleService articleService) {

//		this.articleService = articleService;
	}


//	@GetMapping("/article/all")
//	public ResponseEntity<List<Article>> getAllArticles(){
//		List<Article> articles = articleService.findAllArticles();
//		return new ResponseEntity<>(articles, HttpStatus.OK);
//	}
	
	@GetMapping("/user")
	@PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
	public String userAccess() {
		return "User Content.";
	}

	@GetMapping("/mod")
	@PreAuthorize("hasRole('MODERATOR')")
	public String moderatorAccess() {
		return "Moderator Board.";
	}

	@GetMapping("/admin")
	@PreAuthorize("hasRole('ADMIN')")
	public String adminAccess() {
		return "Admin Board.";
	}
}
